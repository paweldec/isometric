package com.pde.app.editor.main;

import com.alee.laf.WebLookAndFeel;

import javax.swing.*;
import java.util.logging.Logger;

/**
 * Created by pde on 2015-04-09.
 */
public class EditorLauncher {
    private static final Logger log = Logger.getLogger(EditorLauncher.class.getName());

    public static void main (String[] arg) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run () {
                WebLookAndFeel.install();
                new Editor().init();
            }
        });
    }
}
