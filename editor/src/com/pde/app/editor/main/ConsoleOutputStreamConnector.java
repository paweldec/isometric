package com.pde.app.editor.main;

import javax.swing.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Created by Pawel Dec on 2016-01-04.
 */
public class ConsoleOutputStreamConnector extends OutputStream {

    private final JTextArea console;

    private PrintStream stdOut;

    public ConsoleOutputStreamConnector(JTextArea console) {
        this.console = console;
        this.stdOut = System.out;
    }

    public void connect() {
        PrintStream consolePrintStream = new PrintStream(this);
        System.setOut(consolePrintStream);
    }

    @Override
    public void write(int b) throws IOException {
        stdOut.write(b);
        console.append(String.valueOf((char)b));
        console.setCaretPosition(console.getDocument().getLength()-1);
    }
}
