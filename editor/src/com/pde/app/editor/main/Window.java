package com.pde.app.editor.main;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;

import com.pde.app.component.Settings;
import com.pde.app.editor.dialog.OpenModalDialog;
import com.pde.app.editor.dialog.SaveModalDialog;

import javax.swing.*;

import java.awt.*;

/**
 * Created by pde on 2015-04-15.
 */
public class Window extends JFrame {

    private static Window instance;

    private final int leftPanelSize = 200;
    private final int width;
    private final int height;

    // canvas, scrollbars
    public final JPanel rightPanel;
    public LwjglAWTCanvas canvas;
    public final JScrollBar canvasHBar;
    public final JScrollBar canvasVBar;

    // assets etc
    public final JPanel leftPanel;
    public final JTabbedPane tabPanel;
    public final JPanel tilesTabPanel;
    public final JScrollPane tileListScrollPane;
    public final JList tileList;
    public LwjglAWTCanvas tileCanvas;
    public final CanvasPanel tileCanvasPanel;

    // menu
    public final JMenuBar menu;

    public final JMenu menuFile;
    public final JMenuItem menuFileNew;
    public final JMenuItem menuFileOpen;
    public final JMenuItem menuFileSave;
    public final JMenuItem menuFileSaveAs;
    public final JMenuItem menuFileExit;

    public final JMenu menuEdit;
    public final JMenuItem menuEditUndo;
    public final JMenuItem menuEditRedo;

    public final JMenu menuTools;
    public final JMenuItem menuToolsSpriteEditor;

    // console
    public final JPanel southPanel;
    public final JPanel consolePanel;
    public final JScrollPane consoleScrollPane;
    public final JTextArea console;
    public final JTextField consoleInput;

    // status bar
    public final StatusBar statusBar;

    // dialogs
    public final SaveModalDialog saveModalDialog;
    public final OpenModalDialog openModalDialog;

    private Window() {
        setTitle("Game Editor");

        Settings settings = Settings.getInstance();
        settings.load("settings.dat");

        width = settings.getAsInt("resolution.x") + leftPanelSize;
        height = settings.getAsInt("resolution.y");

        rightPanel = new JPanel();
        rightPanel.setLayout(new BorderLayout());

        canvas = new LwjglAWTCanvas(new ApplicationAdapter() {
        });

        rightPanel.add(canvas.getCanvas(), BorderLayout.CENTER, 0);

        canvasHBar = new JScrollBar(JScrollBar.HORIZONTAL, 0, 10, 0, 10);
        canvasVBar = new JScrollBar(JScrollBar.VERTICAL, 0, 10, 0, 10);
        rightPanel.add(canvasHBar, BorderLayout.SOUTH);
        rightPanel.add(canvasVBar, BorderLayout.EAST);

        leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());

        tilesTabPanel = new JPanel();
        tilesTabPanel.setLayout(new BorderLayout());

        tileListScrollPane = new JScrollPane();
        tileList = new JList();
        tileList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tileListScrollPane.getViewport().add(tileList);

        tileCanvasPanel = new CanvasPanel();
        tileCanvas = new LwjglAWTCanvas(new ApplicationAdapter() {
        }, canvas);
        tileCanvas.getCanvas().setSize(leftPanelSize, leftPanelSize);

        tilesTabPanel.add(tileCanvas.getCanvas(), BorderLayout.NORTH, 0);
        tilesTabPanel.add(tileListScrollPane, BorderLayout.CENTER);

        tabPanel = new JTabbedPane();
        tabPanel.addTab("Tiles", tilesTabPanel);

        leftPanel.add(tabPanel, BorderLayout.CENTER);

        menu = new JMenuBar();

        menuFile = new JMenu("File");
        menuFileNew = new JMenuItem("New");
        menuFileOpen = new JMenuItem("Open");
        menuFileSave = new JMenuItem("Save");
        menuFileSaveAs = new JMenuItem("Save As...");
        menuFileExit = new JMenuItem("Exit");

        menuEdit = new JMenu("Edit");
        menuEditUndo = new JMenuItem("Undo");
        menuEditRedo = new JMenuItem("Redo");

        menuTools = new JMenu("Tools");
        menuToolsSpriteEditor = new JMenuItem("Sprite editor");

        menuFile.add(menuFileNew);
        menuFile.add(menuFileOpen);
        menuFile.add(menuFileSave);
        menuFile.add(menuFileSaveAs);
        menuFile.add(menuFileExit);

        menuEdit.add(menuEditUndo);
        menuEdit.add(menuEditRedo);

        menuTools.add(menuToolsSpriteEditor);

        menu.add(menuFile);
        menu.add(menuEdit);
        menu.add(menuTools);

        setJMenuBar(menu);

        consolePanel = new JPanel();
        consolePanel.setLayout(new BorderLayout());

        console = new JTextArea();
        console.setEditable(false);

        consoleScrollPane = new JScrollPane(console, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        consoleScrollPane.setPreferredSize(new Dimension(0, leftPanelSize / 2));

        consoleInput = new JTextField();

        consolePanel.add(consoleScrollPane, BorderLayout.CENTER);
        consolePanel.add(consoleInput, BorderLayout.SOUTH);

        statusBar = new StatusBar();

        southPanel = new JPanel();
        southPanel.setLayout(new BorderLayout());
        southPanel.add(consolePanel, BorderLayout.CENTER);
        southPanel.add(statusBar, BorderLayout.SOUTH);

        final Container container = getContentPane();
        container.setLayout(new BorderLayout());
        container.add(leftPanel, BorderLayout.WEST);
        container.add(rightPanel, BorderLayout.CENTER);
        container.add(southPanel, BorderLayout.SOUTH);

        saveModalDialog = new SaveModalDialog();
        openModalDialog = new OpenModalDialog();

        setPreferredSize(new Dimension(width, height));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }

    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }

    public void setCanvas(Canvas canvas) {
        // main render window
        rightPanel.add(canvas.getCanvas(), BorderLayout.CENTER, 0);
        pack();

        setLocationRelativeTo(null);

        // tile window
        tileCanvas = new LwjglAWTCanvas(tileCanvasPanel, canvas);
        tileCanvas.getCanvas().setSize(leftPanelSize, leftPanelSize);
        tilesTabPanel.add(tileCanvas.getCanvas(), BorderLayout.NORTH, 0);
        pack();
    }

    public static Window getInstance() {
        if(instance == null) {
            instance = new Window();
        }

        return instance;
    }
}
