package com.pde.app.editor.main;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.pde.app.Isometric;
import com.pde.app.component.*;
import com.pde.app.editor.command.CommandManager;
import com.pde.app.editor.command.SetTileCommand;
import com.pde.app.editor.tool.spriteeditor.SpriteEditor;
import com.pde.app.sprite.IsoSprite;
import com.pde.app.utils.Callback;
import com.pde.app.utils.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

/**
 * Created by pde on 2015-04-15.
 */
public class Editor {
    private static final Logger log = LoggerFactory.getLogger(Editor.class.getName());

    private final Window window;
    private final ModelManager modelManager;

    private final Isometric isometric;
    private ResourceManager resourceManager;
    private Camera camera;
    private Map map;
    private CoordSystem coordSystem;
    private CommandManager commandManager;
    private InputManager inputManager;

    // tools
    private SpriteEditor spriteEditor;

    private volatile Point<Integer> mouseCoordHolder;

    public Editor() {
        window = Window.getInstance();
        isometric = Isometric.getInstance();

        spriteEditor = new SpriteEditor();

        modelManager = new ModelManager();
        mouseCoordHolder = new Point<Integer>(0,0);

        setListeners();
    }

    public void init() {
        new ConsoleOutputStreamConnector(window.console).connect();
        window.setVisible(true);
        window.setCanvas(Canvas.getInstance());

        spriteEditor.init();
    }

    private void setListeners() {
        isometric.getCallbackManager().register(Isometric.CALLBACK_AFTER_CREATE, new Callback() {
            @Override
            public void call() {
                resourceManager = ResourceManager.getInstance();
                camera = Camera.getInstance();
                map = Map.getInstance();
                coordSystem = CoordSystem.getInstance();
                commandManager = CommandManager.getInstance();
                inputManager = InputManager.getInstance();

                // events from editor
                window.canvasHBar.addAdjustmentListener(new AdjustmentListener() {
                    @Override
                    public void adjustmentValueChanged(AdjustmentEvent e) {
                        camera.setPosX(e.getValue());
                    }
                });

                window.canvasVBar.addAdjustmentListener(new AdjustmentListener() {
                    @Override
                    public void adjustmentValueChanged(AdjustmentEvent e) {
                        camera.setPosY(e.getValue());
                    }
                });

                window.tileList.addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        if (e.getValueIsAdjusting()) return;
                        window.tileList.getSelectedValue().toString();
                        final String selectedSpriteName = window.tileList.getSelectedValue().toString();
                        window.tileCanvasPanel.setSprite(resourceManager.getSprite(selectedSpriteName));

                        log.info("tile selected: {}", selectedSpriteName);
                    }
                });

                window.menuFileOpen.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        MapFormat mapFormat = window.openModalDialog.open();
                        map.setMapFormat(mapFormat);
                    }
                });

                window.menuFileSave.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        window.saveModalDialog.save(map.getMapFormat());
                    }
                });

                window.menuFileSaveAs.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        window.saveModalDialog.saveAs(map.getMapFormat());
                    }
                });

                window.menuEditUndo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        commandManager.undo();
                        updateUndoRedoButton();
                    }
                });

                window.menuEditRedo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        commandManager.redo();
                        updateUndoRedoButton();
                    }
                });

                window.menuToolsSpriteEditor.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        spriteEditor.show(true);
                    }
                });

                // events from engine
                inputManager.registerInputProcessor(new InputAdapter() {
                    @Override
                    public boolean mouseMoved(int x, int y) {
                        updateStatusBarCoord();
                        return false;
                    }

                    @Override
                    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                        final IsoSprite isoSprite = window.tileCanvasPanel.getSprite();
                        if (isoSprite == null) return false;
                        IsoSprite currentSprite = map.getIsoSprite(isoSprite.getLayer(), mouseCoordHolder.x, mouseCoordHolder.y);
                        SetTileCommand.Model redoModel = new SetTileCommand.Model(isoSprite.getLayer(), mouseCoordHolder.x, mouseCoordHolder.y, isoSprite);
                        SetTileCommand.Model undoModel = new SetTileCommand.Model(isoSprite.getLayer(), mouseCoordHolder.x, mouseCoordHolder.y, currentSprite);
                        commandManager.run(new SetTileCommand(redoModel, undoModel));
                        updateUndoRedoButton();
                        return false;
                    }

                    @Override
                    public boolean keyDown(int keycode) {
                        if(inputManager.isKeysPressed(Input.Keys.CONTROL_LEFT, Input.Keys.Z)) {
                            commandManager.undo();
                            updateUndoRedoButton();
                        }

                        if(inputManager.isKeysPressed(Input.Keys.CONTROL_LEFT, Input.Keys.Y)) {
                            commandManager.redo();
                            updateUndoRedoButton();
                        }

                        return false;
                    }
                });

                updateCanvasSize();
                updateTileList();
                updateStatusBar();
                updateUndoRedoButton();
            }
        });

        isometric.getCallbackManager().register(Isometric.CALLBACK_ON_RESIZE, new Callback() {
            @Override
            public void call() {
                updateCanvasSize();
            }
        });
    }

    private void updateCanvasSize() {
        window.canvasHBar.setMaximum(Math.max(map.getSizeX() - camera.getViewportWidth(), 0));
        window.canvasHBar.setMinimum(CoordSystem.TILE_WIDTH_HALF);
        window.canvasVBar.setMaximum(Math.max(map.getSizeY() - camera.getViewportHeight() - CoordSystem.TILE_HEIGHT, 0));
    }

    private void updateTileList() {
        modelManager.getTileListModel().clear();

        for(IsoSprite isoSprite : resourceManager.getSpritesByCategory(IsoSprite.CATEGORY_TILE)) {
            modelManager.getTileListModel().addElement(isoSprite.getName());
        }

        window.tileList.setModel(modelManager.getTileListModel());
    }

    private void updateStatusBar() {
        window.statusBar.setStatus(StatusBar.Status.READY);
        updateStatusBarCoord();
    }

    private void updateStatusBarCoord() {
        coordSystem.getMouseCoord(mouseCoordHolder);
        window.statusBar.setCoord(mouseCoordHolder.x, mouseCoordHolder.y);
    }

    private void updateUndoRedoButton() {
        window.menuEditUndo.setEnabled(commandManager.hasNextUndo());
        window.menuEditRedo.setEnabled(commandManager.hasNextRedo());
    }

}
