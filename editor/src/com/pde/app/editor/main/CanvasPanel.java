package com.pde.app.editor.main;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pde.app.sprite.IsoSprite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pde on 2015-04-13.
 */
public class CanvasPanel extends ApplicationAdapter {
    private static final Logger log = LoggerFactory.getLogger(CanvasPanel.class);

    private OrthographicCamera camera;
    private SpriteBatch batch;
    private IsoSprite sprite;

    @Override
    public void create() {
        log.debug("creating canvas panel...");

        batch = new SpriteBatch();
        resize(100, 100);
    }

    @Override
    public void resize(int width, int height) {
        camera = new OrthographicCamera(width, height);
        camera.position.set(width / 2, height / 2, 0);
        camera.update();
    }

    @Override
    public void render () {
        batch.setProjectionMatrix(camera.combined);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();

        if(sprite != null) {
            batch.draw(sprite.getCurrentFrameTexture(),
                    camera.position.x - (sprite.getCurrentFrameTexture().getWidth() / 2),
                    camera.position.y - (sprite.getCurrentFrameTexture().getHeight() / 2));
        }

        batch.end();
    }

    public void setSprite(IsoSprite sprite) {
        this.sprite = sprite;
    }

    public IsoSprite getSprite() {
        return sprite;
    }
}
