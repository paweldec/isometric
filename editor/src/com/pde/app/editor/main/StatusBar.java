package com.pde.app.editor.main;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pdec on 2015-11-28.
 */
public class StatusBar extends JLabel {

    public enum Status {
        LOADING,
        READY
    }

    private String status;
    private String coord;

    public StatusBar() {
        super();
        super.setPreferredSize(new Dimension(100, 16));
        super.setHorizontalAlignment(SwingConstants.RIGHT);
        setStatus(Status.LOADING);
        setCoord(0, 0);
    }

    public void setStatus(Status status) {
        switch(status) {
            case READY: {
                this.status = "ready";
                break;
            }
            case LOADING: {
                this.status = "loading";
                break;
            }
            default: {
                this.status = "unk";
            }
        }

        updateLabel();
    }

    public void setCoord(int x, int y) {
        this.coord = String.format("%dx%d", x, y);
        updateLabel();
    }

    private void updateLabel() {
        setText(String.format("Status:[%s] Coord:[%s]", status, coord));
    }
}