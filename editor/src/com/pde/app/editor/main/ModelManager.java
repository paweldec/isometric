package com.pde.app.editor.main;

import javax.swing.*;

/**
 * Created by pde on 2015-04-15.
 */
public class ModelManager {
    private DefaultListModel tileListModel;

    public ModelManager() {
        tileListModel = new DefaultListModel();
    }

    public DefaultListModel getTileListModel() {
        return tileListModel;
    }
}
