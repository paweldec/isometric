package com.pde.app.editor.main;

import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;
import com.pde.app.Isometric;

/**
 * Created by Pawel Dec on 2016-01-28.
 */
public class Canvas extends LwjglAWTCanvas {

    private static Canvas instance;

    public Canvas() {
        super(Isometric.getInstance());
    }

    public static Canvas getInstance() {
        if(instance == null) {
            instance = new Canvas();
        }

        return instance;
    }

}
