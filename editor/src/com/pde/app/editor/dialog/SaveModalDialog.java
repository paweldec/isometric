package com.pde.app.editor.dialog;

import com.google.common.io.Files;
import com.pde.app.component.MapFormat;
import com.pde.app.editor.main.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;

/**
 * Created by Pawel Dec on 2016-01-01.
 */
public class SaveModalDialog extends ModalDialog {

    private static final Logger log = LoggerFactory.getLogger(SaveModalDialog.class);

    private String lastSavedPath;

    public void save(MapFormat mapFormat) {
        if(lastSavedPath == null) {
            saveAs(mapFormat);
        } else {
            saveProject(mapFormat);
        }
    }

    public void saveAs(MapFormat mapFormat) {
        String path = getPath();
        if(path == null) return;
        lastSavedPath = path;
        saveProject(mapFormat);
    }

    private void saveProject(MapFormat mapFormat) {
        mapFormat.save(lastSavedPath + MAP_EXTENSION);
        log.info("Project saved.");
    }

    private String getPath() {
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showSaveDialog(Window.getInstance());

        String file = null;
        String dir = null;

        if(result == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile().getName();
            dir = fileChooser.getCurrentDirectory().toString();

            if(file != null && dir != null) {
                file = Files.getNameWithoutExtension(file);
                return dir + File.separator + file;
            } else {
                log.error("dir or file is null: {}{}", dir, file);
            }
        }

        return null;
    }

}