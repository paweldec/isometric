package com.pde.app.editor.dialog;

import com.google.common.io.Files;
import com.pde.app.component.MapFormat;
import com.pde.app.editor.main.Window;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by Pawel Dec on 2016-01-01.
 */
public class OpenModalDialog extends ModalDialog {

    private static final Logger log = LoggerFactory.getLogger(OpenModalDialog.class);

    public MapFormat open() {
        String path = getPath();
        if(path == null) return null;

        return MapFormat.load(path + MAP_EXTENSION);
    }

    private String getPath() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().endsWith(MAP_EXTENSION);
            }

            @Override
            public String getDescription() {
                return "Iso editor files";
            }
        });

        int result = fileChooser.showOpenDialog(Window.getInstance());

        String file = null;
        String dir = null;

        if(result == JFileChooser.APPROVE_OPTION) {
            file = fileChooser.getSelectedFile().getName();
            dir = fileChooser.getCurrentDirectory().toString();

            if(file != null && dir != null) {
                file = Files.getNameWithoutExtension(file);
                return dir + File.separator + file;
            } else {
                log.error("dir or file is null: {}{}", dir, file);
            }
        }

        return null;
    }

}