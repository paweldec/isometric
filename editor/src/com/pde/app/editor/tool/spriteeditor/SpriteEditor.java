package com.pde.app.editor.tool.spriteeditor;

import com.pde.app.editor.main.Canvas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Pawel Dec on 2016-01-28.
 */
public class SpriteEditor {

    private static final Logger log = LoggerFactory.getLogger(SpriteEditor.class.getName());

    private SpriteEditorWindow window;

    public SpriteEditor() {
        window = SpriteEditorWindow.getInstance();
    }

    public void init() {
        window.setCanvas(Canvas.getInstance());
    }

    public void show(boolean value) {
        window.setVisible(value);
    }
}
