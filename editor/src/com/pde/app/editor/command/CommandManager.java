package com.pde.app.editor.command;

import java.util.*;

/**
 * Created by pdec on 2015-11-29.
 */
public class CommandManager {

    public static abstract class Command<T> {

        protected T redoModel;
        protected T undoModel;

        protected abstract void redo();
        protected abstract void undo();

        protected T getRedoModel() {
            return redoModel;
        }

        protected void setRedoModel(T redoModel) {
            this.redoModel = redoModel;
        }

        protected T getUndoModel() {
            return undoModel;
        }

        protected void setUndoModel(T undoModel) {
            this.undoModel = undoModel;
        }
    }

    private static CommandManager instance;

    private Deque<Command> redoCommandHistory;
    private Deque<Command> undoCommandHistory;

    public CommandManager() {
        redoCommandHistory = new ArrayDeque<Command>();
        undoCommandHistory = new ArrayDeque<Command>();
    }

    public static CommandManager getInstance() {
        if(instance == null) {
            instance = new CommandManager();
        }

        return instance;
    }

    public void run(Command command) {
        command.redo();
        redoCommandHistory.push(command);
    }

    public boolean hasNextUndo() {
        return redoCommandHistory.size() > 0;
    }

    public boolean hasNextRedo() {
        return undoCommandHistory.size() > 0;
    }

    public void undo() {
        if(redoCommandHistory.size() < 1) return;
        Command command = redoCommandHistory.pop();
        if(command == null) return;

        command.undo();
        undoCommandHistory.push(command);
    }

    public void redo() {
        if(undoCommandHistory.size() < 1) return;
        Command command = undoCommandHistory.pop();
        if(command == null) return;

        command.redo();
        redoCommandHistory.push(command);
    }

}
