package com.pde.app.editor.command;

import com.pde.app.component.CoordSystem;
import com.pde.app.component.Map;
import com.pde.app.utils.Point;
import com.pde.app.sprite.IsoSprite;

/**
 * Created by pdec on 2015-11-29.
 */
public class SetTileCommand extends CommandManager.Command<SetTileCommand.Model> {

    public static class Model {
        private int l;
        private int x;
        private int y;
        private IsoSprite isoSprite;

        public Model(int l, int x, int y, IsoSprite isoSprite) {
            this.l = l;
            this.x = x;
            this.y = y;
            this.isoSprite = isoSprite;
        }
    }

    public SetTileCommand(Model redoModel, Model undoModel) {
        this.setRedoModel(redoModel);
        this.setUndoModel(undoModel);
    }

    private void run(Model model) {
        Point<Integer> mouseCoordHolder = new Point<Integer>(0, 0);
        CoordSystem.getInstance().getMouseCoord(mouseCoordHolder);
        Map.getInstance().setMapFragment(model.l, model.x, model.y, model.isoSprite);
    }

    @Override
    protected void redo() {
        run(redoModel);
    }

    @Override
    protected void undo() {
        run(undoModel);
    }

}
