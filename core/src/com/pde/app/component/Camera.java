package com.pde.app.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Pawel on 2015-03-22.
 */
public class Camera implements Updatable {
    private static final float MOVE_SPEED = 2.5f;
    private static final int VIEWPORT_WIDTH_EXTENSION = 4;
    private static final int VIEWPORT_HEIGHT_EXTENSION = 7;

    private static final Logger log = LoggerFactory.getLogger(Camera.class);
    private static Camera instance;

    private OrthographicCamera camera;
    private int viewportWidthHalf;
    private int viewportHeightHalf;

    private Camera() {
        Settings settings = Settings.getInstance();
        int resolutionX = settings.getAsInt("resolution.x");
        int resolutionY = settings.getAsInt("resolution.y");

        resize(resolutionX, resolutionY);
    }

    public static Camera getInstance() {
        if(instance == null) {
            instance = new Camera();
        }

        return instance;
    }

    public void resize(int width, int height) {
        final float lastPosX = (camera != null ? camera.position.x : width / 2);
        final float lastPosY = (camera != null ? camera.position.y : -(height / 2));

        camera = new OrthographicCamera(width, height);
        camera.position.set(lastPosX, lastPosY, 0.0f);
        camera.update();

        viewportWidthHalf = (int)camera.viewportWidth / 2;
        viewportHeightHalf = (int)camera.viewportHeight / 2;
    }

    public int getPosX() {
        return (int)camera.position.x - viewportWidthHalf;
    }

    public int getPosY() {
        return -((int)camera.position.y + viewportHeightHalf);
    }

    public int getViewportWidthInCoords() {
        return ((int)camera.viewportWidth / CoordSystem.TILE_WIDTH) + VIEWPORT_WIDTH_EXTENSION;
    }

    public int getViewportHeightInCoords() {
        return ((int)camera.viewportHeight / CoordSystem.TILE_HEIGHT_HALF) + VIEWPORT_HEIGHT_EXTENSION;
    }

    public int getViewportWidth() {
        return (int)camera.viewportWidth;
    }

    public int getViewportHeight() {
        return (int)camera.viewportHeight;
    }

    public int getViewportWidthOffsetInCoords() {
        return -(VIEWPORT_WIDTH_EXTENSION / 2);
    }

    public int getViewportHeightOffsetInCoords() {
        return -(VIEWPORT_HEIGHT_EXTENSION / 2);
    }

    public final Matrix4 getMatrix() {
        return camera.combined;
    }

    @Override
    public void update(float delta) {
        if(Gdx.input.isKeyPressed(Input.Keys.D)) {
            camera.position.x += MOVE_SPEED;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
            camera.position.x -= MOVE_SPEED;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.W)) {
            camera.position.y += MOVE_SPEED;
        }

        if(Gdx.input.isKeyPressed(Input.Keys.S)) {
            camera.position.y -= MOVE_SPEED;
        }

        camera.update();
    }

    public void setPosX(int x) {
        camera.position.x = (x + viewportWidthHalf);
    }

    public void setPosY(int y) {
        camera.position.y = (-y - viewportHeightHalf);
    }
}
