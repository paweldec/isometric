package com.pde.app.component;

import com.badlogic.gdx.Gdx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawel on 2015-03-22.
 */
public class UpdateManager {
    private static final Logger log = LoggerFactory.getLogger(UpdateManager.class);
    private static UpdateManager instance;
    private List<List<Updatable>> updateData;

    private static final int UPDATE_LIST_SIZE = 50;

    public class UpdatePriority {
        public static final int CAMERA = 0;
        public static final int SPRITE = 1;
        public static final int TOTAL = 2;
    }

    private UpdateManager() {
        updateData = new ArrayList<List<Updatable>>(UpdatePriority.TOTAL);

        for(int i = 0; i < UpdatePriority.TOTAL; i++) {
            updateData.add(i, new ArrayList<Updatable>(UPDATE_LIST_SIZE));
        }
    }

    public static final UpdateManager getInstance() {
        if(instance == null) {
            instance = new UpdateManager();
        }

        return instance;
    }

    public boolean register(Updatable updatableToRegister, int priority) {
        if(priority < 0 || priority >= updateData.size()) return false;
        updateData.get(priority).add(updatableToRegister);
        return true;
    }

    public boolean unregister(Updatable updatableToUnregister) {
        for(List<Updatable> listToUpdate : updateData) {
            int i = 0;

            for(Updatable updatable : listToUpdate) {
                if(updatable == updatableToUnregister) {
                    listToUpdate.remove(i);
                    return true;
                }

                i++;
            }
        }

        return false;
    }

    public boolean unregister(Updatable updatableToUnregister, int priority) {
        if(priority < 0 || priority >= updateData.size()) return false;
        return updateData.get(priority).remove(updatableToUnregister);
    }

    public void update() {
        float delta = Math.min(Gdx.graphics.getRawDeltaTime(), 1/30);

        for(List<Updatable> updateList : updateData) {
            for(Updatable updatable : updateList) {
                updatable.update(delta);
            }
        }
    }

}
