package com.pde.app.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pde on 2015-04-01.
 */
public class MapFormat implements Serializable {
    private static final Logger log = LoggerFactory.getLogger(MapFormat.class);

    private int width;
    private int height;
    private int layers;
    private MapFragment[][][] fragments = null;
    private List<String> spriteMapping;

    public MapFormat(int layers, int width, int height) {
        if(layers < 1 || width < 1 || height < 1) {
            throw new IllegalArgumentException("MapFormat illegal size or num of layers");
        }

        this.width = width;
        this.height = height;
        this.layers = layers;

        fragments = new MapFragment[layers][height][width];

        for(int l = 0; l < layers; l++) {
            for(int y = 0; y < height; y++) {
                for(int x = 0; x < width; x++) {
                    fragments[l][y][x] = new MapFragment();
                    if(l == 0) fragments[l][y][x].spriteId = 1;
                }
            }
        }

        spriteMapping = new ArrayList<String>();
        spriteMapping.add("none");
        spriteMapping.add("m1_grass1");
    }

    public final MapFragment getMapFragment(int l, int x, int y) {
        if(l < 0) return null;
        if(y < 0) return null;
        if(x < 0) return null;
        if(l >= fragments.length) return null;
        if(y >= fragments[0].length) return null;
        if(x >= fragments[0][0].length) return null;

        return fragments[l][y][x];
    }

    public final List<String> getSpriteMapping() {
        return spriteMapping;
    }

    public final int getWidth() {
        return width;
    }

    public final int getHeight() {
        return height;
    }

    public final int getTotalLayers() {
        return layers;
    }

    public boolean save(String path) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
            log.info("map saved {}", path);
        } catch (Exception e) {
            log.error("map save error", e);
        }

        return true;
    }

    public static MapFormat load(String path) {
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            MapFormat mapFormat = (MapFormat)in.readObject();
            in.close();
            fileIn.close();
            log.info("map loaded {}", path);
            return mapFormat;
        } catch(Exception e) {
            log.error("map load error", e);
        }

        return null;
    }
}
