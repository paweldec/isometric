package com.pde.app.component;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by Pawel on 2015-03-21.
 */
public interface Renderable {
    public void render(SpriteBatch spriteBatch);
}
