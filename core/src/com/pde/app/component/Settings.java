package com.pde.app.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Properties;

/**
 * Created by Pawel on 2015-03-20.
 */
public class Settings {
    private static final Logger log = LoggerFactory.getLogger(Settings.class);
    private static Settings instance;

    private Properties properties;

    private Settings() {}

    public static Settings getInstance() {
        if(instance == null) {
            instance = new Settings();
        }

        return instance;
    }

    public void load(String path) {
        properties = new Properties();
        properties.setProperty("resolution.x", "800");
        properties.setProperty("resolution.y", "600");

        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(new File(path));

            try {
                log.info("Loading settings file '{}'", path);
                properties.load(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            log.warn("Settings file '{}' not found!", path);
            return;
        }
    }

    public String getAsString(String key) {
        return properties.getProperty(key);
    }

    public int getAsInt(String key) {
        return Integer.parseInt(properties.getProperty(key));
    }
}
