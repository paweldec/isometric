package com.pde.app.component;

/**
 * Created by Pawel on 2015-03-21.
 */
public interface Updatable {
    public void update(float delta);
}
