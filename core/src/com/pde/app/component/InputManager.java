package com.pde.app.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pdec on 2015-11-28.
 */
public class InputManager {
    private static InputManager instance;
    private InputMultiplexer inputMultiplexer;

    private Map<Integer, Boolean> keys;

    private InputManager() {
        inputMultiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(inputMultiplexer);

        keys = new HashMap<Integer, Boolean>();

        registerInputProcessor(new InputProcessor() {

            @Override
            public boolean keyDown(int keycode) {
                keys.put(keycode, Boolean.TRUE);
                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                keys.put(keycode, Boolean.FALSE);
                return false;
            }

            @Override
            public boolean keyTyped(char character) {
                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        });
    }

    public void registerInputProcessor(InputProcessor inputProcessor) {
        inputMultiplexer.addProcessor(inputProcessor);
    }

    public void unregisterInputProcessor(InputProcessor inputProcessor) {
        inputMultiplexer.removeProcessor(inputProcessor);
    }

    public static final InputManager getInstance() {
        if(instance == null) {
            instance = new InputManager();
        }

        return instance;
    }

    public boolean isKeyPressed(int keyCode) {
        if(keys.containsKey(keyCode) && keys.get(keyCode).equals(true)) {
            return true;
        }

        return false;
    }

    public boolean isKeysPressed(int ... keyCodes) {
        for(int keyCode : keyCodes) {
            if(!isKeyPressed(keyCode)) {
                return false;
            }
        }

        return true;
    }
}
