package com.pde.app.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawel on 2015-03-21.
 */
public class RenderManager {
    private static final Logger log = LoggerFactory.getLogger(RenderManager.class);
    private static RenderManager instance;

    private static final int RENDER_LIST_SIZE = 50;

    public class RenderPriority {
        public static final int MAP = 0;
        public static final int TOTAL = 1;
    }

    private List<List<Renderable>> renderData;
    private final Camera camera = Camera.getInstance();
    private SpriteBatch batch;

    private RenderManager() {
        renderData = new ArrayList<List<Renderable>>(RenderPriority.TOTAL);

        for(int i = 0; i < RenderPriority.TOTAL; i++) {
            renderData.add(i, new ArrayList<Renderable>(RENDER_LIST_SIZE));
        }

        batch = new SpriteBatch();
    }

    public static final RenderManager getInstance() {
        if(instance == null) {
            instance = new RenderManager();
        }

        return instance;
    }

    public boolean register(Renderable renderableToRegister, int priority) {
        if(priority < 0 || priority >= renderData.size()) return false;
        renderData.get(priority).add(renderableToRegister);
        return true;
    }

    public boolean unregister(Renderable renderableToUnregister) {
        for(List<Renderable> listToRender : renderData) {
            int i = 0;

            for(Renderable renderable : listToRender) {
                if(renderable == renderableToUnregister) {
                    listToRender.remove(i);
                    return true;
                }

                i++;
            }
        }

        return false;
    }

    public boolean unregister(Renderable renderableToUnregister, int priority) {
        if(priority < 0 || priority >= renderData.size()) return false;
        return renderData.get(priority).remove(renderableToUnregister);
    }

    public void render() {
        batch.setProjectionMatrix(camera.getMatrix());

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();

        for(List<Renderable> listToRender : renderData) {
            for(Renderable renderable : listToRender) {
                renderable.render(batch);
            }
        }

        batch.end();
    }
}
