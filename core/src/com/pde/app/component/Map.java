package com.pde.app.component;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.pde.app.sprite.IsoSprite;
import com.pde.app.utils.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 * Created by Pawel on 2015-03-22.
 */
public class Map implements Renderable {
    private static final Logger log = LoggerFactory.getLogger(Map.class);
    private static Map instance;

    private final CoordSystem coordSystem;
    private final Camera camera;
    private final ResourceManager resourceManager;

    private MapFormat mapFormat = new MapFormat(3, 50, 60);
    private java.util.Map<Integer, IsoSprite> mappedSprites = new HashMap<Integer, IsoSprite>();

    private IsoSprite selection;
    private Point<Integer> _mouseCoord;

    private Map() {
        coordSystem = CoordSystem.getInstance();
        camera = Camera.getInstance();
        resourceManager = ResourceManager.getInstance();

        selection = resourceManager.getSprite("selection");

        refreshSpriteMapping();

        _mouseCoord = new Point<Integer>(0, 0);
    }

    private void refreshSpriteMapping() {
        for(int i = 0; i < mapFormat.getSpriteMapping().size(); i++) {
            String spriteName = mapFormat.getSpriteMapping().get(i);
            IsoSprite sprite = resourceManager.getSprite(spriteName);

            if(sprite != null) {
                mappedSprites.put(i, sprite);
            }
        }
    }

    public static Map getInstance() {
        if(instance == null) {
            instance = new Map();
        }

        return instance;
    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        coordSystem.getMouseCoord(_mouseCoord);

        for(int l = 0; l < mapFormat.getTotalLayers(); l++) {
            for (int y = 0; y < camera.getViewportHeightInCoords(); y++) {
                for (int x = 0; x < camera.getViewportWidthInCoords(); x++) {
                    int mapIndexX = x + (camera.getPosX() / CoordSystem.TILE_WIDTH) + camera.getViewportWidthOffsetInCoords();
                    int mapIndexY = y + (camera.getPosY() / CoordSystem.TILE_HEIGHT_HALF) + camera.getViewportHeightOffsetInCoords();

                    final MapFragment mapFragment = mapFormat.getMapFragment(l, mapIndexX, mapIndexY);
                    if (mapFragment == null) continue;

                    IsoSprite sprite = mappedSprites.get(mapFragment.spriteId);
                    if(sprite == null) continue;

                    float tileCordX = (mapIndexX * CoordSystem.TILE_WIDTH) + (mapIndexY % 2 == 0 ? CoordSystem.TILE_WIDTH_HALF : 0);
                    float tileCordY = (mapIndexY * CoordSystem.TILE_HEIGHT_HALF);
                    spriteBatch.draw(sprite.getCurrentFrameTexture(), tileCordX, -tileCordY);

                    if (mapIndexX == _mouseCoord.x && mapIndexY == _mouseCoord.y) {
                        spriteBatch.draw(selection.getCurrentFrameTexture(), tileCordX, -tileCordY);
                    }
                }
            }
        }
    }

    public IsoSprite getIsoSprite(int l, int x, int y) {
        MapFragment mapFragment = mapFormat.getMapFragment(l, x, y);
        if(mapFragment == null) return null;
        return mappedSprites.get(mapFragment.spriteId);
    }

    public void setMapFragment(int l, int x, int y, IsoSprite isoSprite) {
        MapFragment mapFragment = mapFormat.getMapFragment(l, x, y);
        if(mapFragment == null) return;
        boolean spriteFound = false;

        int spriteId = -1;
        int i = 0;

        if(isoSprite != null) {
            for (String spriteName : mapFormat.getSpriteMapping()) {
                if (spriteName.equals(isoSprite.getName())) {
                    spriteFound = true;
                    spriteId = i;
                    break;
                }

                i++;
            }

            if(!spriteFound) {
                mapFormat.getSpriteMapping().add(isoSprite.getName());
                spriteId = mapFormat.getSpriteMapping().size() - 1;
                mappedSprites.put(spriteId, isoSprite);
            }
        }

        mapFragment.spriteId = spriteId > 0 ? spriteId : 1;
    }

    public int getSizeX() {
        return mapFormat.getWidth() * CoordSystem.TILE_WIDTH;
    }

    public int getSizeY() {
        return mapFormat.getHeight() * CoordSystem.TILE_HEIGHT_HALF;
    }

    public MapFormat getMapFormat() {
        return mapFormat;
    }

    public synchronized void setMapFormat(MapFormat mapFormat) {
        if(mapFormat == null) return;
        this.mapFormat = mapFormat;
        refreshSpriteMapping();
    }

}
