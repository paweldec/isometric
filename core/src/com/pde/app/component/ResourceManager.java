package com.pde.app.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.pde.app.sprite.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Pawel on 2015-03-22.
 */
public class ResourceManager {
    private static final Logger log = LoggerFactory.getLogger(ResourceManager.class);

    private static ResourceManager instance;

    private final Map<String, TextureAtlas> atlases;
    private final Map<String, Sprite> textures;
    private final Map<String, IsoSprite> sprites;

    private ResourceManager() {
        atlases = new HashMap<String, TextureAtlas>();
        textures = new HashMap<String, Sprite>();
        sprites = new HashMap<String, IsoSprite>();

        loadAll();
    }

    public static final ResourceManager getInstance() {
        if(instance == null) {
            instance = new ResourceManager();
        }

        return instance;
    }

    private void loadAll() {
        List<File> spriteFiles = IsoSpriteLoader.getAllSpriteFiles("sprites\\");

        for(File file : spriteFiles) {
            log.info(String.format("loading sprite: %s", file.getPath()));
            IsoSprite isoSprite = IsoSpriteLoader.load(file);

            if(!atlases.containsKey(isoSprite.getAtlasName())) {
                String atlasPath = String.format("atlases\\%s.pack", isoSprite.getAtlasName());
                log.info(String.format("loading atlas: %s", atlasPath));
                TextureAtlas atlas = new TextureAtlas(Gdx.files.internal(atlasPath));
                atlases.put(isoSprite.getAtlasName(), atlas);
            }

            for(Animation animation : isoSprite.getAnimations().values()) {
                for(Frame frame : animation.getFrames()) {
                    if(!textures.containsKey(frame.getTextureName())) {
                        log.info(String.format("loading texture: %s", frame.getTextureName()));
                        Sprite sprite = atlases.get(isoSprite.getAtlasName()).createSprite(frame.getTextureName());
                        textures.put(frame.getTextureName(), sprite);
                        frame.init(new IsoTexture(sprite));
                    }
                }
            }

            sprites.put(isoSprite.getName(), isoSprite);
        }
    }

    public IsoSprite getSprite(String name) {
        return sprites.get(name);
    }

    public List<IsoSprite> getAllSprites() {
        return new ArrayList<IsoSprite>(sprites.values());
    }

    public List<IsoSprite> getSpritesByCategory(String ... categories) {
        List<IsoSprite> filteredSprites = new ArrayList<IsoSprite>();

        for(IsoSprite sprite : sprites.values()) {
            for (String category : categories) {
                if (sprite.getCategory().contains(category)) {
                    filteredSprites.add(sprite);
                    break;
                }
            }
        }

        return filteredSprites;
    }
}
