package com.pde.app.component;

import com.badlogic.gdx.Gdx;
import com.pde.app.utils.SimpleMath;
import com.pde.app.utils.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pde on 2015-03-20.
 */
public class CoordSystem {
    public static final int TILE_WIDTH = 64;
    public static final int TILE_HEIGHT = 32;
    public static final int TILE_WIDTH_HALF = TILE_WIDTH / 2;
    public static final int TILE_HEIGHT_HALF = TILE_HEIGHT / 2;

    private static final Logger log = LoggerFactory.getLogger(CoordSystem.class);

    private static CoordSystem instance;
    private final Camera camera = Camera.getInstance();

    private CoordSystem() {

    }

    public static final CoordSystem getInstance() {
        if(instance == null) {
            instance = new CoordSystem();
        }

        return instance;
    }

    public int getMousePosX() {
        int posX = Gdx.input.getX() + camera.getPosX();
        return posX > 0 ? posX : 0;
    }

    public int getMouseRelativePosX() {
        return Gdx.input.getX();
    }

    public int getMouseRelativePosY() {
        return Gdx.input.getY();
    }

    public int getMousePosY() {
        int posY = Gdx.input.getY() + camera.getPosY();
        return posY > 0 ? posY : 0;
    }

    public void posToCoord(final int posX, final int posY, final Point<Integer> coord) {
        int normalizedPosX = posX % (TILE_WIDTH);
        int normalizedPosY = posY % (TILE_HEIGHT);

        int coordX = (int)Math.floor(posX / (TILE_WIDTH));
        int coordY = (int)Math.floor(posY / (TILE_HEIGHT)) * 2;

        //System.out.println("lewy gorny");
        if(SimpleMath.isInsideTriangle(normalizedPosX, normalizedPosY,
                0 - 1, TILE_HEIGHT_HALF - 1, 0 - 1, TILE_HEIGHT + 1, TILE_WIDTH_HALF + 1, TILE_HEIGHT + 1)) {
            coordY++;
        }
        //System.out.println("prawy gorny");
        else if(SimpleMath.isInsideTriangle(normalizedPosX, normalizedPosY,
                TILE_WIDTH_HALF - 1, TILE_HEIGHT + 1, TILE_WIDTH + 1, TILE_HEIGHT + 1, TILE_WIDTH + 1, TILE_HEIGHT_HALF - 1)) {
            coordX++;
            coordY++;
        }
        //System.out.println("lewy dolny");
        else if(SimpleMath.isInsideTriangle(normalizedPosX, normalizedPosY,
                0 - 1, 0 - 1, 0 - 1, TILE_HEIGHT_HALF + 1, TILE_WIDTH_HALF + 1, 0 - 1)) {
            coordY--;
        }
        //System.out.println("prawy dolny");
        else if(SimpleMath.isInsideTriangle(normalizedPosX, normalizedPosY,
                TILE_WIDTH + 1, TILE_HEIGHT_HALF + 1, TILE_WIDTH + 1, 0 - 1, TILE_WIDTH_HALF - 1, 0 - 1)) {
            coordX++;
            coordY--;
        }

        coord.x = coordX;
        coord.y = coordY;
    }

    public void coordToPos(final Point<Integer> coord, final Point<Integer> pos) {
        pos.x = (coord.x * TILE_WIDTH) + (coord.y % 2 == 0 ? TILE_WIDTH_HALF : 0);
        pos.y = (coord.y * TILE_HEIGHT_HALF);
    }

    public void getMouseCoord(final Point<Integer> coord) {
        posToCoord(getMousePosX() - TILE_WIDTH_HALF, getMousePosY() + TILE_HEIGHT, coord);
    }

    public void getPointerPos(final Point<Integer> pos) {
        getMouseCoord(pos);
        coordToPos(pos, pos);
    }
}
