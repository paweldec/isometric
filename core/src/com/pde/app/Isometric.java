package com.pde.app;

import com.badlogic.gdx.ApplicationAdapter;
import com.pde.app.component.*;
import com.pde.app.utils.CallbackManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Isometric extends ApplicationAdapter {
    public static final String CALLBACK_AFTER_CREATE = "AFTER_CREATE";
    public static final String CALLBACK_ON_RESIZE = "ON_RESIZE";

    private static final Logger log = LoggerFactory.getLogger(Isometric.class);

    private static Isometric instance;

    private ResourceManager resourceManager;
    private Camera camera;
    private CoordSystem coordSystem;
    private UpdateManager updateManager;
    private RenderManager renderManager;
    private Map map;
    private InputManager inputManager;

    private CallbackManager callbackManager;

    private Isometric() {
        super();
        callbackManager = new CallbackManager();
    }

    public static Isometric getInstance() {
        if(instance == null) {
            instance = new Isometric();
        }

        return instance;
    }

	@Override
	public void create () {
        log.info("engine starting...");

        log.info("init resource manager");
        resourceManager = ResourceManager.getInstance();
        log.info("init camera");
        camera = Camera.getInstance();
        log.info("init coord system");
        coordSystem = CoordSystem.getInstance();
        log.info("init update manager");
        updateManager = UpdateManager.getInstance();
        log.info("init render manager");
        renderManager = RenderManager.getInstance();
        log.info("init map manager");
        map = Map.getInstance();
        log.info("init input manager");
        inputManager = InputManager.getInstance();

        log.info("register map to render");
        renderManager.register(map, RenderManager.RenderPriority.MAP);

        log.info("register camera to update");
        updateManager.register(camera, UpdateManager.UpdatePriority.CAMERA);

        log.info("engine ready!");

        callbackManager.call(CALLBACK_AFTER_CREATE);
	}

	@Override
	public void render () {
        updateManager.update();
        renderManager.render();
	}
	
	@Override
	public void resize(int width, int height) {
		camera.resize(width, height);

        callbackManager.call(CALLBACK_ON_RESIZE);
	}

    public final CallbackManager getCallbackManager() {
        return callbackManager;
    }
}
