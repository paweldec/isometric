package com.pde.app.utils;

import java.util.HashMap;

/**
 * Created by pde on 2015-04-13.
 */
public class CallbackManager {
    private HashMap<String, Callback> callbacks;

    public CallbackManager() {
        callbacks = new HashMap<String, Callback>();
    }

    public void register(String event, Callback callback) {
        if(event == null || callback == null) return;
        callbacks.put(event, callback);
    }

    public void call(String event) {
        if(callbacks.containsKey(event)) {
            callbacks.get(event).call();
        }
    }
}
