package com.pde.app.utils;

/**
 * Created by Pawel on 2015-03-22.
 */
public final class Point<T> {

    public Point(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public T x;
    public T y;

    public String toString() {
        return String.format("[%s, %s]", x.toString(), y.toString());
    }
}
