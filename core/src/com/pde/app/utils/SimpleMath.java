package com.pde.app.utils;

/**
 * Created by pde on 2015-03-20.
 */
public class SimpleMath {
    public static boolean isInsideTriangle(int testPointX, int testPointY,
                                     int tPointX1, int tPointY1, int tPointX2, int tPointY2, int tPointX3, int tPointY3) {

        int planeAB = (tPointX1 - testPointX) * (tPointY2 - testPointY) - (tPointX2 - testPointX) * (tPointY1 - testPointY);
        int planeBC = (tPointX2 - testPointX) * (tPointY3 - testPointY) - (tPointX3 - testPointX) * (tPointY2 - testPointY);
        int planeCA = (tPointX3 - testPointX) * (tPointY1 - testPointY) - (tPointX1 - testPointX) * (tPointY3 - testPointY);

        return sign(planeAB) == sign(planeBC) && sign(planeBC) == sign(planeCA);
    }

    public static int sign(int value) {
        return value == 0 ? 0 : Math.abs(value) / value;
    }

    private static int round(int value, int base) {
        return Math.round(value / base) * base;
    }
}
