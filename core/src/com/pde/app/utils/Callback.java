package com.pde.app.utils;

/**
 * Created by pde on 2015-04-09.
 */
public interface Callback {
    void call();
}
