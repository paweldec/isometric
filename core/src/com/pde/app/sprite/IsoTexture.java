package com.pde.app.sprite;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by pde on 2015-04-17.
 */
// iso engine have own sprite class so we override original Sprite
public class IsoTexture extends Sprite {
    public IsoTexture() {
        super();
    }

    public IsoTexture(Sprite sprite) {
        super(sprite);
    }

    public IsoTexture(Texture texture) {
        super(texture);
    }

    public IsoTexture(Texture texture, int srcWidth, int srcHeight) {
        super(texture, srcWidth, srcHeight);
    }

    public IsoTexture(Texture texture, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(texture, srcX, srcY, srcWidth, srcHeight);
    }

    public IsoTexture(TextureRegion textureRegion) {
        super(textureRegion);
    }

    public IsoTexture(TextureRegion region, int srcX, int srcY, int srcWidth, int srcHeight) {
        super(region, srcX, srcY, srcWidth, srcHeight);
    }
}
