package com.pde.app.sprite;

/**
 * Created by Pawel on 2015-04-16.
 */
public class Frame {
    private final String textureName;

    private transient IsoTexture texture;
    private transient boolean init;

    public Frame(String textureName) {
        this.textureName = textureName;
    }

    public void init(IsoTexture texture) {
        this.texture = texture;
        init = true;
    }

    public String getTextureName() {
        return textureName;
    }

    public IsoTexture getTexture() {
        return texture;
    }
}
