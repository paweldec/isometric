package com.pde.app.sprite;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import com.eclipsesource.json.JsonValue;
import com.google.common.io.Files;
import com.google.common.base.Charsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pde on 2015-04-17.
 */
// It will be replaced by binary serialization
public class IsoSpriteLoader {
    private static final Logger log = LoggerFactory.getLogger(IsoSpriteLoader.class);

    public static IsoSprite load(File spriteFile) {
        try {
            JsonObject jsonObject = JsonObject.readFrom(Files.toString(spriteFile, Charsets.UTF_8));
            JsonObject isoSpriteJson = jsonObject.get("isosprite").asObject();

            String name = isoSpriteJson.get("name").asString();
            String atlasName = isoSpriteJson.get("atlasName").asString();
            String category = isoSpriteJson.get("category").asString();
            JsonValue layerValue = isoSpriteJson.get("layer");
            int layer = layerValue != null ? layerValue.asInt() : 0;
            String defaultAnimation = isoSpriteJson.get("defaultAnimation").asString();
            int offsetX = isoSpriteJson.get("offsetX").asInt();
            int offsetY = isoSpriteJson.get("offsetY").asInt();

            IsoSprite isoSprite = new IsoSprite(name, atlasName);
            isoSprite.setCategory(category);
            isoSprite.setLayer(layer);
            isoSprite.setDefaultAnimation(defaultAnimation);
            isoSprite.setOffsetX(offsetX);
            isoSprite.setOffsetY(offsetY);

            JsonArray animationArrayJson = isoSpriteJson.get("animations").asArray();

            for(int i = 0; i < animationArrayJson.size(); i++) {
                JsonObject animationJson = animationArrayJson.get(i).asObject();

                String animName = animationJson.get("name").asString();
                boolean animLoop = animationJson.get("loop").asBoolean();
                int delay = animationJson.get("delay").asInt();

                Animation animation = new Animation(animName);
                animation.setLoop(animLoop);
                animation.setDelay(delay);

                JsonArray frameArrayJson = animationJson.get("frames").asArray();

                for(int j = 0; j < frameArrayJson.size(); j++) {
                    JsonObject frameJson = frameArrayJson.get(j).asObject();

                    String frameTextureName = frameJson.get("textureName").asString();
                    Frame frame = new Frame(frameTextureName);
                    animation.addFrame(frame);
                }

                isoSprite.addAnimation(animation);
            }

            return isoSprite;
        }
        catch (Exception e) {
            e.printStackTrace();
            log.warn("Cannot load IsoSprite: {}", spriteFile.getPath());
        }

        return null;
    }

    public static List<File> getAllSpriteFiles(String path) {
        File dir = new File(path);
        File filesInDir[] = dir.listFiles();
        List<File> spriteFiles = new ArrayList<File>();

        for(File file : filesInDir) {
            if(!file.isFile()) continue;
            if(!Files.getFileExtension(file.getName()).equals("sprite")) continue;

            spriteFiles.add(file);
        }

        return spriteFiles;
    }
}

/*
{
  "isosprite": {
    "name": "player",
    "atlasPath": "images/demo_level.pack",
	"defaultAnimation": "walkLeft",
	"offsetX": "0",
	"offsetY": "0",
    "animations": [
      {
        "name": "walkLeft",
        "loop": "true",
        "delay": "50",
        "frames": [
          { "textureName": "bobwalkleft01" },
          { "textureName": "bobwalkleft02" },
          { "textureName": "bobwalkleft03" },
          { "textureName": "bobwalkleft04" },
          { "textureName": "bobwalkleft05" }
        ]
      },
      {
        "name": "walkRight",
        "loop": "true",
        "delay": "50",
        "frames": [
          { "textureName": "bobwalkright01" },
          { "textureName": "bobwalkright02" },
          { "textureName": "bobwalkright03" },
          { "textureName": "bobwalkright04" },
          { "textureName": "bobwalkright05" }
        ]
      },
      {
        "name": "idleLeft",
        "loop": "false",
        "delay": "50",
        "frames": [
		  { "textureName": "bobidleleft01" }
		]
      }
    ]
  }
}
 */