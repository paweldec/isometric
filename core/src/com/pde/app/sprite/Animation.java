package com.pde.app.sprite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pawel on 2015-04-16.
 */
public class Animation {
    private final String name;
    private boolean loop;
    private int delay;
    private final List<Frame> frames;

    private transient long lastTimeStamp;
    private transient int currentFrameIndex;

    public Animation(String name) {
        this.name = name;
        frames = new ArrayList<Frame>();

        lastTimeStamp = System.currentTimeMillis();
    }

    public String getName() {
        return name;
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public void addFrame(Frame frame) {
        if(frame == null) return;
        frames.add(frame);
    }

    public Frame getFrame(int index) {
        if(frames.size() < 1) return null;
        if(frames.size() < index || index < 0) return frames.get(0);
        return frames.get(index);
    }

    public List<Frame> getFrames() {
        return frames;
    }

    public Frame getCurrentFrame() {
        return frames.get(currentFrameIndex);
    }

    public void resetFrame() {
        currentFrameIndex = 0;
    }

    public void update() {
        if((lastTimeStamp + delay) < System.currentTimeMillis()) {
            if(frames.size() - 1 > currentFrameIndex) {
                currentFrameIndex++;
            }
            else {
                if(loop) {
                    currentFrameIndex = 0;
                }
            }
        }
    }
}
