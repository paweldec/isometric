package com.pde.app.sprite;

import com.pde.app.component.Updatable;
import com.pde.app.component.UpdateManager;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pawel on 2015-04-16.
 */
public class IsoSprite implements Updatable {
    public transient static final String CATEGORY_TILE = "tile";
    public transient static final String CATEGORY_OTHER = "other";

    private transient final UpdateManager updateManager;

    private final String name;
    private final String atlasName;
    private String category;
    private int layer;
    private String defaultAnimation;
    private int offsetX;
    private int offsetY;
    private final Map<String, Animation> animations;

    private transient Animation currentAnimation;

    public IsoSprite(String name, String atlasName) {
        updateManager = UpdateManager.getInstance();

        this.name = name;
        this.atlasName = atlasName;

        animations = new HashMap<String, Animation>();
    }

    public boolean setUpdate(boolean update) {
        if(update) {
            return updateManager.register(this, UpdateManager.UpdatePriority.SPRITE);
        }
        else {
            return updateManager.unregister(this, UpdateManager.UpdatePriority.SPRITE);
        }
    }

    public final IsoTexture getCurrentFrameTexture() {
        return currentAnimation.getCurrentFrame().getTexture();
    }

    public boolean setAnimation(String name) {
        if(animations.containsKey(name)) {
            currentAnimation = animations.get(name);
            currentAnimation.resetFrame();
            return true;
        }

        return false;
    }

    public void setDefaultAnimation() {
        setAnimation(defaultAnimation);
    }

    @Override
    public void update(float delta) {
        if(currentAnimation == null) return;
        currentAnimation.update();
    }

    public String getName() {
        return name;
    }

    public String getAtlasName() {
        return atlasName;
    }

    public String getDefaultAnimation() {
        return defaultAnimation;
    }

    public void setDefaultAnimation(String defaultAnimation) {
        this.defaultAnimation = defaultAnimation;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(int offsetX) {
        this.offsetX = offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(int offsetY) {
        this.offsetY = offsetY;
    }

    public final Map<String, Animation> getAnimations() {
        return animations;
    }

    public void addAnimation(Animation animation) {
        animations.put(animation.getName(), animation);

        if(animations.containsKey(defaultAnimation)) {
            currentAnimation = animations.get(defaultAnimation);
        }
        else {
            currentAnimation = animation;
            currentAnimation.resetFrame();
        }
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }
}
