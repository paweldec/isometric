package com.pde.app.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.pde.app.Isometric;
import com.pde.app.component.Settings;

public class DesktopLauncher {
	public static void main (String[] arg) {
        Settings settings = Settings.getInstance();
        settings.load("settings.dat");

        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = settings.getAsInt("resolution.x");
        config.height = settings.getAsInt("resolution.y");

		new LwjglApplication(new Isometric(), config);
	}
}
